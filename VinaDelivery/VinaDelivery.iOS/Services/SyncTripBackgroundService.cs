﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using Plugin.Connectivity;
using Prism.Ioc;
using UIKit;
using VinaDelivery.Services;
using Xamarin.Forms;

namespace VinaDelivery.iOS.Services
{
    class SyncTripBackgroundService
    {
        nint _taskId;
        CancellationTokenSource _cts;
        private bool _isRunning;

        public async Task Start()
        {
            if (CrossConnectivity.Current.IsConnected && !_isRunning)
            {
                _isRunning = true;

                _cts = new CancellationTokenSource();

                _taskId = UIApplication.SharedApplication.BeginBackgroundTask("SyncTripBackgroundService", OnExpiration);

                try
                {
                    var syncService = App.Current.Container.Resolve<ISyncService>();
                    await syncService.SyncTripAsync();

                    _isRunning = false;
                }
                catch (OperationCanceledException)
                {
                }
                finally
                {
                    if (_cts.IsCancellationRequested)
                    {

                    }
                }

                UIApplication.SharedApplication.EndBackgroundTask(_taskId);
            }
        }

        public void Stop()
        {
            _isRunning = false;
            _cts.Cancel();
        }

        void OnExpiration()
        {
            _isRunning = false;
            _cts.Cancel();
        }
    }
}