﻿using System;
using System.Collections.Generic;
using System.Linq;
using FFImageLoading.Forms.Touch;
using Foundation;
using UIKit;
using VinaDelivery;
using SuaveControls.FloatingActionButton.iOS.Renderers;
using Microsoft.AppCenter.Push;
using VinaDelivery.iOS.Services;
using VinaDelivery.Utils;
using Xamarin.Forms;

namespace VinaDelivery.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Xamarin.FormsMaps.Init();
            CachedImageRenderer.Init();
            FloatingActionButtonRenderer.InitRenderer();
            Rg.Plugins.Popup.Popup.Init();

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(UIApplication.BackgroundFetchIntervalMinimum);

            RegisterOnDemandSyncService();

            return base.FinishedLaunching(app, options);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, System.Action<UIBackgroundFetchResult> completionHandler)
        {
            var result = Push.DidReceiveRemoteNotification(userInfo);
            if (result)
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NewData);
            }
            else
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NoData);
            }
        }

        public override void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            var service = new SyncTripBackgroundService();
            service.Start().Wait();

            // Inform system of fetch results
            completionHandler(UIBackgroundFetchResult.NewData);
        }

        private void RegisterOnDemandSyncService()
        {
            MessagingCenter.Subscribe<SyncTripMessage>(this, "SyncTrip", async message =>
            {
                var service = new SyncTripBackgroundService();
                await service.Start();
            });
        }
    }
}
