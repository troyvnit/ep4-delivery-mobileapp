﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SQLite;
using VinaDelivery.Data;
using VinaDelivery.iOS.Providers;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteProvider))]
namespace VinaDelivery.iOS.Providers
{
    public class SQLiteProvider : ISQLiteProvider
    {
        private string GetDatabasePath(string databaseName)
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            //var libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
            var path = Path.Combine(documentsPath, databaseName);
            return path;
        }

        /// <summary>
        /// Get SQlite connection
        /// </summary>
        /// <param name="databaseName">If database name contains "/", assume it is a full database path</param>
        /// <returns></returns>
        public SQLiteConnection GetConnection(string databaseName)
        {
            SQLiteConnection conn;
            if (databaseName.Contains("/"))
            {
                conn = new SQLiteConnection(databaseName);
            }
            else
            {
                var path = GetDatabasePath(databaseName);
                conn = new SQLiteConnection(path);
            }

            return conn;
        }
    }
}
