﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using VinaDelivery.iOS.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("MyEffect")]
[assembly: ExportEffect(typeof(AppEntryEffect), "AppEntryEffect")]
namespace VinaDelivery.iOS.Effects
{
    public class AppEntryEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            ((UITextField)Control).BorderStyle = UITextBorderStyle.None;
        }

        protected override void OnDetached()
        {
        }
    }
}
