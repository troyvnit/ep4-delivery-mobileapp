﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Connectivity;
using Prism.Ioc;
using VinaDelivery.Services;

namespace VinaDelivery.Droid.Services
{
    [Service]
    class SyncTripBackgroundService : Service
    {
        private CancellationTokenSource _cts;
        private bool _isRunning;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (CrossConnectivity.Current.IsConnected && !_isRunning)
            {
                _isRunning = true;

                _cts = new CancellationTokenSource();

                Task.Run(() => {
                    try
                    {
                        var syncService = App.Current.Container.Resolve<ISyncService>();
                        syncService.SyncTripAsync().Wait();
                        
                        _isRunning = false;
                    }
                    catch (Android.OS.OperationCanceledException)
                    {
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    finally
                    {
                        if (_cts.IsCancellationRequested)
                        {
                        }
                    }
                }, _cts.Token);
            }

            return StartCommandResult.Sticky;
        }

        public override void OnDestroy()
        {
            _isRunning = false;

            if (_cts != null)
            {
                _cts.Token.ThrowIfCancellationRequested();
                _cts.Cancel();
            }

            base.OnDestroy();
        }
    }
}