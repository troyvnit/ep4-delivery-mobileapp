﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using FFImageLoading.Forms.Droid;
using Microsoft.AppCenter.Push;
using Naxam.Controls.Platform.Droid;
using Plugin.Iconize;
using Plugin.LocalNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using VinaDelivery;
using VinaDelivery.Droid.Services;
using VinaDelivery.Utils;
using VinaDelivery.Views.Account;

namespace VinaDelivery.Droid
{
    [Activity(Label = "Vina Delivery", Icon = "@drawable/app_logo", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            UserDialogs.Init(this);
            Xamarin.FormsMaps.Init(this, bundle);
            SetupBottomTabs();
            Iconize.Init(Resource.Id.toolbar, Resource.Id.sliding_tabs);
            CachedImageRenderer.Init(true);
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            Push.SetSenderId("500009110158");

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            SetAlarmForBackgroundServices(this);
            RegisterOnDemandSyncService();
        }

        void SetupBottomTabs()
        {
            var stateList = new Android.Content.Res.ColorStateList(
                new int[][] {
                    new int[] { Android.Resource.Attribute.StateChecked
                    },
                    new int[] { Android.Resource.Attribute.StateEnabled
                    }
                },
                new int[] {
                    Color.FromHex("13C8C3").ToAndroid(), //Selected 
                    Color.FromHex("73899C").ToAndroid() //Normal
                });

            BottomTabbedRenderer.BackgroundColor = Color.White.ToAndroid();
            BottomTabbedRenderer.FontSize = 12f;
            BottomTabbedRenderer.IconSize = 20f;
            BottomTabbedRenderer.ItemTextColor = stateList;
            BottomTabbedRenderer.ItemIconTintList = stateList;
            BottomTabbedRenderer.ItemSpacing = 4;
            BottomTabbedRenderer.BottomBarHeight = 60;
            BottomTabbedRenderer.ItemAlign = ItemAlignFlags.Center;
            BottomTabbedRenderer.ShouldUpdateSelectedIcon = true;
            BottomTabbedRenderer.MenuItemIconSetter = (menuItem, iconSource, selected) =>
            {
                var iconized = Iconize.FindIconForKey(iconSource.File);
                if (iconized == null)
                {
                    BottomTabbedRenderer.DefaultMenuItemIconSetter.Invoke(menuItem, iconSource, selected);

                    return;
                }

                var drawable = new IconDrawable(this, iconized).Color(selected ? Color.FromHex("13C8C3").ToAndroid() : Color.FromHex("73899C").ToAndroid()).SizeDp(45);

                menuItem.SetIcon(drawable);
            };

            LocalNotificationsImplementation.NotificationIconId = Resource.Drawable.app_logo;
        }

        private void RegisterOnDemandSyncService()
        {
            MessagingCenter.Subscribe<SyncTripMessage>(this, "SyncTrip", message =>
            {
                var backgroundServiceIntent = new Intent(this, typeof(SyncTripBackgroundService));
                StartService(backgroundServiceIntent);
            });
        }

        public static void SetAlarmForBackgroundServices(Context context)
        {
            var alarmIntent = new Intent(context.ApplicationContext, typeof(AlarmReceiver));
            var pendingIntent = PendingIntent.GetBroadcast(context.ApplicationContext, 0, alarmIntent, PendingIntentFlags.UpdateCurrent);
            var alarmManager = (AlarmManager)context.GetSystemService(Context.AlarmService);
            alarmManager.SetInexactRepeating(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime(), 60000, pendingIntent);
        }

        protected override void OnNewIntent(Android.Content.Intent intent)
        {
            base.OnNewIntent(intent);
            Push.CheckLaunchedFromNotification(this, intent);
        }
    }
}

