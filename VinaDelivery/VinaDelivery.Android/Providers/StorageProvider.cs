using System;
using System.IO;
using VinaDelivery.Droid.Providers;
using VinaDelivery.Providers;
using Xamarin.Forms;

[assembly: Dependency(typeof(StorageProvider))]
namespace VinaDelivery.Droid.Providers
{
    public class StorageProvider : IStorageProvider
    {
        public bool FileExist(string filePath)
        {
            return File.Exists(filePath);
        }

        public string SaveFile(string src)
        {
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var des = Path.Combine(documents, Path.GetFileName(src));

            File.Copy(src, des);

            return des;
        }

        public string SaveFile(Stream stream, string fileName)
        {
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var des = Path.Combine(documents, fileName);

            stream.Position = 0;
            using (var memStream = new MemoryStream())
            {
                stream.CopyTo(memStream);
                var bytes = memStream.ToArray();

                File.WriteAllBytes(des, bytes);
            }

            return des;
        }

        public byte[] ReadFile(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }
    }
}