﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using VinaDelivery.Data;
using VinaDelivery.Droid.Providers;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteProvider))]
namespace VinaDelivery.Droid.Providers
{
    public class SQLiteProvider : ISQLiteProvider
    {
        private string GetDatabasePath(string databaseName)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, databaseName);
            return path;
        }

        /// <summary>
        /// Get SQlite connection
        /// </summary>
        /// <param name="databaseName">If database name contains "/", assume it is a full database path</param>
        /// <returns></returns>
        public SQLiteConnection GetConnection(string databaseName)
        {
            SQLiteConnection conn;
            if (databaseName.Contains("/"))
            {
                conn = new SQLiteConnection(databaseName);
            }
            else
            {
                var path = GetDatabasePath(databaseName);
                conn = new SQLiteConnection(path);
            }

            return conn;
        }
    }
}