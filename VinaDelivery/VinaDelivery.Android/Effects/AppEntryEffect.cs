﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using VinaDelivery.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("MyEffect")]
[assembly: ExportEffect(typeof(AppEntryEffect), "AppEntryEffect")]
namespace VinaDelivery.Droid.Effects
{
    public class AppEntryEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var nativeEditText = (EditText)Control;
            nativeEditText.SetBackgroundColor(Color.Transparent.ToAndroid());
        }

        protected override void OnDetached()
        {
        }
    }
}