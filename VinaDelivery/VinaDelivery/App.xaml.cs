﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Push;
using Plugin.Connectivity;
using Plugin.Iconize;
using Plugin.LocalNotifications;
using Prism.Autofac;
using Prism.Ioc;
using Prism.Navigation;
using VinaDelivery.Data;
using VinaDelivery.FontModules;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Services.Identity;
using VinaDelivery.Utils;
using VinaDelivery.Views;
using VinaDelivery.Views.Account;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;

namespace VinaDelivery
{
	public partial class App : PrismApplication
	{
	    public new static App Current => (App)Application.Current;

        public static bool IsNetworkReachable;

	    public TripModel Trip;

        public Guid? DeviceId;

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
	    {
            // Pages
            containerRegistry.RegisterForNavigation<RootPage>();
	        containerRegistry.RegisterForNavigation<MasterPage>();
            containerRegistry.RegisterForNavigation<LoginPage>();
	        containerRegistry.RegisterForNavigation<TripPreparationPage>();
	        containerRegistry.RegisterForNavigation<TripConfirmationPage>();
	        containerRegistry.RegisterForNavigation<TripTrackingPage>();
	        containerRegistry.RegisterForNavigation<TripTrackingListPage>();
	        containerRegistry.RegisterForNavigation<TripTrackingMapPage>();
	        containerRegistry.RegisterForNavigation<TripTrackingDetailPage>();
	        containerRegistry.RegisterForNavigation<TripTrackingDeliveryPage>();
            containerRegistry.RegisterForNavigation<TripDeliveryPage>();

            // Services
            containerRegistry.RegisterSingleton<IAppService, AppService>();
	        containerRegistry.RegisterSingleton<IIdentityService, IdentityService>();
            containerRegistry.RegisterSingleton<IRequestProvider, RequestProvider>();
	        containerRegistry.RegisterSingleton<IAuthRequestProvider, AuthRequestProvider>();
            containerRegistry.RegisterSingleton<ITripService, TripService>();
	        containerRegistry.RegisterSingleton<IAppDataStorage, AppDataStorage>();
	        containerRegistry.RegisterSingleton<ISyncService, SyncService>();
        }

	    protected override void OnInitialized()
	    {
            InitializeComponent();

	        // Network status
	        IsNetworkReachable = CrossConnectivity.Current.IsConnected;
	        CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
	        {
	            IsNetworkReachable = args.IsConnected;
	        };
            
            // Iconize
	        Iconize
	            .With(new Plugin.Iconize.Fonts.MaterialModule())
	            .With(new Plugin.Iconize.Fonts.SimpleLineIconsModule())
	            .With(new FontTello("Logistics", "Logistics", "logistics.ttf", new List<IIcon>() {
	                new Icon("lgts-package-i", '\ue828'),
	                new Icon("lgts-package-ii", '\ue80e'),
	                new Icon("lgts-reaching-destination", '\ue813'),
	                new Icon("lgts-delivery-checklist", '\ue810'),
	                new Icon("lgts-loaded-truck", '\ue805'),
	                new Icon("lgts-time-based-delivery", '\ue80a'),
	                new Icon("lgts-receive-package", '\ue827'),
	                new Icon("lgts-heavy-truck", '\ue82e'),
	                new Icon("lgts-fast-delivery", '\ue81e'),
	                new Icon("lgts-boxes", '\ue81a')
	            }));

            // AppCenter Notifications
	        Push.PushNotificationReceived += (sender, e) => {

	            //// Add the notification message and title to the message
	            //var summary = $"Push notification received:" +
	            //                    $"\n\tNotification title: {e.Title}" +
	            //                    $"\n\tMessage: {e.Message}";

	            //// If there is custom data associated with the notification,
	            //// print the entries
	            //if (e.CustomData != null)
	            //{
	            //    summary += "\n\tCustom data:\n";
	            //    foreach (var key in e.CustomData.Keys)
	            //    {
	            //        summary += $"\t\t{key} : {e.CustomData[key]}\n";
	            //    }
	            //}

	            //// Send the notification summary to debug output
	            //System.Diagnostics.Debug.WriteLine(summary);

	            CrossLocalNotifications.Current.Show(e.Title, e.Message);
	        };

            // AppCenter
            AppCenter.Start("android=4feeff35-23f0-4b42-b9f6-8dcb15cc6f37;" + "ios=6e3ba6a4-a2de-4818-80c4-514d3ef20b0e",
	            typeof(Analytics), typeof(Crashes), typeof(Push));

            MessagingCenter.Subscribe<AuthRequestProvider>(this, "Logout", provider =>
            {
                Settings.ClearAuthentication();
                MainPage = new LoginPage();
            });

	        var appDataStorage = Container.Resolve<IAppDataStorage>();
	        var appData = appDataStorage.GetAppData();
	        if (appData.CurrentTrip == null)
	        {
	            MainPage = new LoginPage();
	        }
	        else
	        {
                // if trip expired, sync new trip
                if (appData.SyncDate < DateTime.Now.Date)
	            {
	                Trip = null;
	                appData.CurrentTrip = null;
	                appData.CurrentCustomerDeliveryId = 0;
	                appData.FinishedStep = null;
	                appData.IsCurrentTripCompleted = false;
	                appData.NeedSync = false;
                    appDataStorage.SaveAppData(appData);
                    MainPage = new LoginPage();
	            }
	            else
	            {
	                Trip = appData.CurrentTrip;
                    Trip.CustomerDeliveryItems.OrganizeCustomerDeliveries();

                    // otherwise, go to the current step
                    var mainPage = new RootPage();

                    var finishedStep = appData.FinishedStep;
	                if (appData.IsCurrentTripCompleted)
	                {
	                    mainPage.Detail = new IconNavigationPage(new TripDonePage());
                    }
	                else if (string.IsNullOrEmpty(finishedStep))
	                {
	                    mainPage.Detail = new IconNavigationPage(new TripPreparationPage());
	                }
	                else if (finishedStep == "Confirmation")
	                {
	                    mainPage.Detail = new IconNavigationPage(new TripTrackingPage());
                    }
	                
	                MainPage = mainPage;
	            }
	        }
        }

	    protected override void OnStart()
	    {
        }

	    protected override void OnSleep()
	    {
	        // Handle when your app sleeps
	    }

        protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
