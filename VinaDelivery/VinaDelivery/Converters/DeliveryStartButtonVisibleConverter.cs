﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class DeliveryStartButtonVisibleConverter : IValueConverter
    {
        public static DeliveryStartButtonVisibleConverter Instance = new DeliveryStartButtonVisibleConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (CustomerDeliveryStatusEnum) value;
            if (status == CustomerDeliveryStatusEnum.Pending)
                return true;

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
