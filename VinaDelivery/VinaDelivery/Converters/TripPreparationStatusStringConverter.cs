﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class TripPreparationStatusStringConverter : IValueConverter
    {
        public static TripPreparationStatusStringConverter Instance = new TripPreparationStatusStringConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return "Bắt đầu giao hàng";

            return "Chưa nhận đủ hàng";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}