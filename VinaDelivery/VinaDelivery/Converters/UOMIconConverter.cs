﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class UOMIconConverter : IValueConverter
    {
        public static UOMIconConverter Instance = new UOMIconConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value?.ToString() == "5")
            {
                return "lgts-package-i";
            }

            return "lgts-package-ii";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
