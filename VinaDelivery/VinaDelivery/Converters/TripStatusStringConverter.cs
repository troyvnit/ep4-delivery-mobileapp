﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    class TripStatusStringConverter : IValueConverter
    {
        public static TripStatusStringConverter Instance = new TripStatusStringConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return "✓ Đã nhận";

            return "Nhận hàng";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}