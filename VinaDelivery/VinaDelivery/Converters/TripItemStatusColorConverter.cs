﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class TripItemStatusColorConverter : IValueConverter
    {
        public static TripItemStatusColorConverter Instance = new TripItemStatusColorConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool) value)
                return Color.FromHex("13C8C3");

            return Color.FromHex("FFBD24");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
