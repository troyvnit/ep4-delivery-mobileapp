﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class HighlightConverter : IValueConverter
    {
        public static HighlightConverter Instance = new HighlightConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(bool) value)
                return Color.Red;

            return Color.FromHex("73899C");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
