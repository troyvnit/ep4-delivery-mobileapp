﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class DeliveryStatusConverter : IValueConverter
    {
        public static DeliveryStatusConverter Instance = new DeliveryStatusConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (CustomerDeliveryStatusEnum) value;
            if (status == CustomerDeliveryStatusEnum.Pending)
                return Color.FromHex("73899C");

            if (status == CustomerDeliveryStatusEnum.Started)
                return Color.FromHex("009CDB");

            if (status == CustomerDeliveryStatusEnum.PartialDone)
                return Color.FromHex("FFBD24");

            if (status == CustomerDeliveryStatusEnum.Failed)
                return Color.Red;

            return Color.FromHex("13C8C3");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
