﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace VinaDelivery.Converters
{
    public class TripPreparationStatusColorConverter : IValueConverter
    {
        public static TripPreparationStatusColorConverter Instance = new TripPreparationStatusColorConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return Color.FromHex("13C8C3");

            return Color.FromHex("9E9E9E");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}