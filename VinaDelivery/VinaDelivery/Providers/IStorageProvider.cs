﻿using System.IO;

namespace VinaDelivery.Providers
{
    public interface IStorageProvider
    {
        bool FileExist(string filePath);
        string SaveFile(string src);
        string SaveFile(Stream stream, string fileName);
        byte[] ReadFile(string filePath);
    }
}