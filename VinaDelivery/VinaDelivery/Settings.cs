﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace VinaDelivery
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        private const string ACCESS_TOKEN_KEY = "AccessToken";
        private const string ACCESS_TOKEN_EXPIRES_DATE = "AccessTokenExpiresDate";
        private const string USERNAME = "Username";
        private const string PASSWORD = "Password";
        private const string FULL_NAME = "FullName";

        public static string AccessToken
        {
            get => AppSettings.GetValueOrDefault(ACCESS_TOKEN_KEY, null);
            set => AppSettings.AddOrUpdateValue(ACCESS_TOKEN_KEY, value);
        }

        public static DateTime AccessTokenExpiresDate
        {
            get => AppSettings.GetValueOrDefault(ACCESS_TOKEN_EXPIRES_DATE, new DateTime(1, 1, 1));
            set => AppSettings.AddOrUpdateValue(ACCESS_TOKEN_EXPIRES_DATE, value);
        }

        public static string FullName
        {
            get => AppSettings.GetValueOrDefault(FULL_NAME, null);
            set => AppSettings.AddOrUpdateValue(FULL_NAME, value);
        }

        public static string Username
        {
            get => AppSettings.GetValueOrDefault(USERNAME, null);
            set => AppSettings.AddOrUpdateValue(USERNAME, value);
        }

        public static string Password
        {
            get => AppSettings.GetValueOrDefault(PASSWORD, null);
            set => AppSettings.AddOrUpdateValue(PASSWORD, value);
        }

        public static void ClearAuthentication()
        {
            AccessToken = null;
            AccessTokenExpiresDate = new DateTime(1, 1, 1);
            Username = null;
            Password = null;
            FullName = null;
        }
    }
}
