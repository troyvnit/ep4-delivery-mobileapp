﻿using Plugin.Iconize;
using System.Collections.Generic;

namespace VinaDelivery.FontModules
{
    public class FontTello : IconModule
    {
        public FontTello(string fontFamily, string fontName, string fontPath, IEnumerable<IIcon> icons) : base(fontFamily, fontName, fontPath, icons)
        {
        }
    }
}
