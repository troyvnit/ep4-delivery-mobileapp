﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using VinaDelivery.Controls;
using System.Collections.ObjectModel;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using Newtonsoft.Json;

namespace VinaDelivery.Models
{
    public class TripModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public DriverModel Driver { get; set; }

        public VehicleModel Vehicle { get; set; }

        public SupervisorModel Supervisor { get; set; }

        public TripStatus Status { get; set; }

        public string StopReason { get; set; }

        public TripStatusEnum StatusEnum => (TripStatusEnum)Status.Id;

        public List<DeliveryModel> Deliveries { get; set; }

        public List<DeliveryItemModel> PickedDeliveryItems { get; set; } = new List<DeliveryItemModel>();

        [JsonIgnore]
        public ObservableCollection<CustomerDeliveryViewModel> CustomerDeliveryItems { get; set; } = new ObservableCollection<CustomerDeliveryViewModel>();

        public string RowVersionString { get; set; }
    }

    public class DeliveryModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public CustomerModel Customer { get; set; }

        public string CustomerName => Customer.Name;

        public DeliveryStatus Status { get; set; }

        public int StatusId { get; set; }

        public DeliveryStatusEnum StatusEnum => (DeliveryStatusEnum)Status.Id;

        [JsonIgnore]
        public CustomerDeliveryStatusEnum TypeEnum => (CustomerDeliveryStatusEnum)Status.Id;

        public bool IsFailed => StatusEnum == DeliveryStatusEnum.Failed;

        public string Address { get; set; }

        public DateTime? AvailableDeliveryTime { get; set; }

        public DateTime? EstimatedDeliveryTime { get; set; }

        public List<DeliveryItemModel> Items { get; set; }

        public string Note { get; set; }

        public string SignatureFileName { get; set; }

        public List<GroupingList<int, DeliveryItemModel>> GroupingItems { get; set; } = new List<GroupingList<int, DeliveryItemModel>>();

        public string RowVersionString { get; set; }
    }

    public class DeliveryItemModel : BindableBase
    {
        public int Id { get; set; }

        public ProductModel Product { get; set; }

        private int _deliveredQuantity;
        public int DeliveredQuantity {
            get => _deliveredQuantity;
            set {
                IsFullDelivered = value == Quantity;
                SetProperty(ref _deliveredQuantity, value);
            }
        }

        private bool _isFullDeliveried;
        public bool IsFullDelivered
        {
            get => _isFullDeliveried;
            set => SetProperty(ref _isFullDeliveried, value);
        }

        public int PickupQuantity { get; set; }

        public int Quantity { get; set; }

        public string QuantityDisplay => $"{PickupQuantity}/{Quantity}";

        public string Lot { get; set; }

        public string WarehouseName { get; set; }

        public string WarehouseCode { get; set; }

        public DateTime LotDate { get; set; }

        private bool _isDone;
        public bool IsDone
        {
            get => _isDone;
            set => SetProperty(ref _isDone, value);
        }

        private bool _isDeliveryDone;
        public bool IsDeliveryDone
        {
            get => _isDeliveryDone;
            set => SetProperty(ref _isDeliveryDone, value);
        }

        public string Deliveries { get; set; }

        public string RowVersionString { get; set; }
    }

    public class TripStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class DeliveryStatus
    {
        public DeliveryStatus() { }
        public DeliveryStatus(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public enum TripStatusEnum
    {
        Awaiting = 1,
        Pending = 2,
        OnTrip = 3,
        Stop = 4,
        Completed = 5
    }

    public enum DeliveryStatusEnum
    {
        Pending = 1,
        PartialDone = 2,
        Done = 3,
        Failed = 4,
        Picked = 5,
        OnDelivery = 6
    }

    public enum CustomerDeliveryStatusEnum
    {
        Pending = 1,
        Started = 2,
        PartialDone = 3,
        Done = 4,
        Failed = 5
    }

    public class UploadSignatureResult
    {
        public string FileName { get; set; }
    }
}
