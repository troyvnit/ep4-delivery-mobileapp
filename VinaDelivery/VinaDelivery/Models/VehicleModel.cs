﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinaDelivery.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }

        public string PlateNumber { get; set; }

        public string TransportPartner { get; set; }

        public VehicleType Type { get; set; }

        [JsonIgnore]
        public VehicleTypeEnum TypeEnum => (VehicleTypeEnum) Type.Id;

        public string RowVersionString { get; set; }
    }

    public class VehicleType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public enum VehicleTypeEnum
    {
        Bike = 1,
        Truck = 2
    }
}
