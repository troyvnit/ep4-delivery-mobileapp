﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinaDelivery.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string ContactPerson { get; set; }

        public string Address { get; set; }

        public string Notes { get; set; }

        public string Phone { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public string RowVersionString { get; set; }
    }
}
