﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinaDelivery.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string BarCode { get; set; }

        public string Picture { get; set; }

        public string SerialNum { get; set; }

        public decimal Weight { get; set; }

        public decimal Height { get; set; }

        public string UoMCode { get; set; }

        public string UoMName { get; set; }

        public string RowVersionString { get; set; }
    }
}
