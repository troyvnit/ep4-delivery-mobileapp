﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Controls;
using VinaDelivery.Views.Account;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage : ContentPage
	{
	    public string FullName { get; set; }

	    public string Username { get; set; }

	    public ObservableCollection<MasterPageItem> Items { get; set; }
        public Command<MasterPageItem> ExecuteItemCommand { get; set; }

		public MasterPage ()
		{
			InitializeComponent ();

            Init();

		    BindingContext = this;
		}

	    public void Init()
	    {
	        FullName = Settings.FullName;
	        Username = Settings.Username;

            Items = new ObservableCollection<MasterPageItem>
	        {
	            new MasterPageItem { Title = "Tài Khoản", IconSource = "account.png", Action = () => {
                }},
                new MasterPageItem { Title = "Cấu Hình", IconSource = "settings.png", Action = () => {
                }},
	            new MasterPageItem { Title = "Đăng Xuất", Action = () =>
	            {
	                Settings.ClearAuthentication();
                    App.Current.MainPage =  new LoginPage();
	            }}
            };

            ExecuteItemCommand = new Command<MasterPageItem>((x) => { x.Action(); });
        }
	}
}