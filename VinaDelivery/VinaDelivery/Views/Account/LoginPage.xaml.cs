﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.ViewModels.Account;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views.Account
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private readonly LoginPageViewModel _viewModel;

        public LoginPage()
        {
            InitializeComponent();

            _viewModel = (LoginPageViewModel)BindingContext;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (!string.IsNullOrEmpty(Settings.AccessToken))
            {
                await _viewModel.LoadDataAsync();
            }
        }
    }
}