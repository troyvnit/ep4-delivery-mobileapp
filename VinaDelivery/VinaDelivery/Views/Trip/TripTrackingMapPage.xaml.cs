﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Prism.Ioc;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripTrackingMapPage : ContentPage
	{
	    private readonly TripTrackingMapPageViewModel _viewModel;

        public TripTrackingMapPage ()
		{
			InitializeComponent ();

		    _viewModel = App.Current.Container.Resolve<TripTrackingMapPageViewModel>();
		    BindingContext = _viewModel;
		}

	    protected override async void OnAppearing()
	    {
	        base.OnAppearing();

	        var locator = CrossGeolocator.Current;
	        var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

	        var map = new Map(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromKilometers(3)))
	        {
	            IsShowingUser = true,
	            VerticalOptions = LayoutOptions.FillAndExpand
	        };

	        foreach (var delivery in App.Current.Trip.CustomerDeliveryItems.Where(item => item.IsPreparationDone))
	        {
	            map.Pins.Add(new Pin()
	            {
	                Label = delivery.Customer.Name,
	                Address = delivery.Customer.Address,
	                Type = PinType.Place,
	                Position = new Position(double.Parse(delivery.Customer.Latitude.ToString()), double.Parse(delivery.Customer.Longitude.ToString()))
	            });
	        }

	        MapWrapper.Children.Clear();
	        MapWrapper.Children.Add(map);
	    }
    }
}