﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Ioc;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripTrackingListPage : ContentPage
	{
	    private readonly TripTrackingListPageViewModel _viewModel;

		public TripTrackingListPage ()
		{
			InitializeComponent ();

		    _viewModel = App.Current.Container.Resolve<TripTrackingListPageViewModel>();
		    BindingContext = _viewModel;
		}

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        _viewModel.LoadData();
        }
	}
}