﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Prism.Ioc;
using VinaDelivery.Models;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripTrackingDetailPage : ContentPage
	{
	    private readonly TripTrackingDetailPageViewModel _viewModel;
	    private readonly CustomerDeliveryViewModel _delivery;

	    public TripTrackingDetailPage(CustomerDeliveryViewModel delivery)
	    {
	        InitializeComponent();

	        _delivery = delivery;
	        _viewModel = (TripTrackingDetailPageViewModel)BindingContext;
            _viewModel.LoadData(_delivery);
	    }
    }
}