﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naxam.Controls.Forms;
using Prism.Ioc;
using VinaDelivery.Data;
using VinaDelivery.Models;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripTrackingPage : BottomTabbedPage
    {
        private readonly TripTrackingPageViewModel _viewModel;

        public TripTrackingPage ()
        {
            InitializeComponent ();

            _viewModel = App.Current.Container.Resolve<TripTrackingPageViewModel>();
            BindingContext = _viewModel;
        }

        //protected override async void OnAppearing()
        //{
        //    base.OnAppearing();

        //    var appDataStorage = App.Current.Container.Resolve<IAppDataStorage>();
        //    var appData = appDataStorage.GetAppData();
        //    var currentCustomerDelivery = App.Current.Trip.CustomerDeliveryItems.FirstOrDefault(x => x.Id == appData.CurrentCustomerDeliveryId);
        //    if (currentCustomerDelivery != null)
        //    {
        //        if (currentCustomerDelivery.Status == CustomerDeliveryStatusEnum.Pending)
        //        {
        //            await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripTrackingDeliveryPage(currentCustomerDelivery));
        //        }
        //        else if (currentCustomerDelivery.Status == CustomerDeliveryStatusEnum.Started)
        //        {
        //            await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripDeliveryPage(currentCustomerDelivery));
        //        }
        //    }
        //}
    }
}