﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Ioc;
using VinaDelivery.Models;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripTrackingDeliveryPage : ContentPage
	{
        private readonly TripTrackingDeliveryPageViewModel _viewModel;
	    private readonly CustomerDeliveryViewModel _delivery;

		public TripTrackingDeliveryPage (CustomerDeliveryViewModel delivery)
		{
			InitializeComponent ();

		    _delivery = delivery;
		    _viewModel = (TripTrackingDeliveryPageViewModel)BindingContext;
		    _viewModel.LoadData(_delivery);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

            var map = new Map(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromKilometers(3)))
            {
                IsShowingUser = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            var delivery = _viewModel.Delivery;
            if (delivery != null && !string.IsNullOrEmpty(delivery.Customer.Address) && delivery.Customer.Latitude > 0 && delivery.Customer.Longitude > 0)
            {
                map.Pins.Add(new Pin()
                {
                    Label = delivery.Customer.Name,
                    Address = delivery.Customer.Address,
                    Type = PinType.Place,
                    Position = new Position(double.Parse(delivery.Customer.Latitude.ToString()), double.Parse(delivery.Customer.Longitude.ToString()))
                });
            }

            MapWrapper.Children.Clear();
            MapWrapper.Children.Add(map);
        }
    }
}