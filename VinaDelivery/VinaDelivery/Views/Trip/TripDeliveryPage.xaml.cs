﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Ioc;
using VinaDelivery.Models;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripDeliveryPage : ContentPage
	{
        private readonly TripDeliveryPageViewModel _viewModel;

		public TripDeliveryPage(CustomerDeliveryViewModel delivery)
		{
			InitializeComponent ();
            
		    _viewModel = (TripDeliveryPageViewModel)BindingContext;
		    _viewModel.LoadData(delivery);
        }
    }
}