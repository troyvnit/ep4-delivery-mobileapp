﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.ViewModels.Trip;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VinaDelivery.Views.Trip
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripPreparationPage : ContentPage
	{
	    private readonly TripPreparationPageViewModel _viewModel;

		public TripPreparationPage ()
		{
			InitializeComponent ();

		    _viewModel = (TripPreparationPageViewModel) BindingContext;
		}

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        _viewModel.LoadData();
        }
	}
}