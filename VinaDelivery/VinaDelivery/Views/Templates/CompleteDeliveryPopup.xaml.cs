﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.ViewModels.Templates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Templates
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CompleteDeliveryPopup : PopupPage
	{
        private readonly CompleteDeliveryPopupViewModel _viewModel;
        private readonly CustomerDeliveryViewModel _delivery;

        public CompleteDeliveryPopup (CustomerDeliveryViewModel delivery)
		{
			InitializeComponent ();

            _delivery = delivery;
            _viewModel = (CompleteDeliveryPopupViewModel)BindingContext;
            _viewModel.Delivery = delivery;
        }

        private async Task OnComplete(object sender, EventArgs e)
        {
            _viewModel.SignatureImageStream = await padView.GetImageStreamAsync(SignaturePad.Forms.SignatureImageFormat.Png);
            _viewModel.CompleteCommand.Execute();
        }
    }
}