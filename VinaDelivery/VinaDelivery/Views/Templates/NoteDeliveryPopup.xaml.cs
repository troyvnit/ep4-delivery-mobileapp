﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.ViewModels.Templates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Templates
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NoteDeliveryPopup : PopupPage
	{
        private readonly NoteDeliveryPopupViewModel _viewModel;

        public NoteDeliveryPopup (CustomerDeliveryViewModel delivery, NoteFor noteFor = NoteFor.Normal)
		{
			InitializeComponent ();
            
            _viewModel = (NoteDeliveryPopupViewModel)BindingContext;
            _viewModel.Delivery = delivery;
            _viewModel.NoteFor = noteFor;
        }

        private void NoteChanged(object sender, EventArgs e)
        {
            _viewModel.Note = ((Editor)sender).Text;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.Note = _viewModel.Delivery.Note;
        }
    }
}