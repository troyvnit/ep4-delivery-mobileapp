﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeliveryEditView : ContentView
    {
        public static readonly BindableProperty CustomerDeliveryItemProperty = BindableProperty.Create(
            propertyName: "CustomerDeliveryItem",
            returnType: typeof(CustomerDeliveryViewModel),
            declaringType: typeof(DeliveryDetailView),
            defaultValue: null,
            defaultBindingMode: BindingMode.OneWay
        );

        public CustomerDeliveryViewModel CustomerDeliveryItem
        {
            get { return (CustomerDeliveryViewModel)GetValue(CustomerDeliveryItemProperty); }
            set { SetValue(CustomerDeliveryItemProperty, value); }
        }

        public DeliveryEditView()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public void Done(object sender, EventArgs e)
        {

        }
    }
}