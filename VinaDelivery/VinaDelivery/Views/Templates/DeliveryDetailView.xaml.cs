﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeliveryDetailView : ContentView
    {
        public static readonly BindableProperty CustomerDeliveryItemProperty = BindableProperty.Create(
            propertyName: "CustomerDeliveryItem",
            returnType: typeof(CustomerDeliveryViewModel),
            declaringType: typeof(DeliveryDetailView),
            defaultValue: null,
            defaultBindingMode: BindingMode.OneWay
        );

        public CustomerDeliveryViewModel CustomerDeliveryItem
        {
            get { return (CustomerDeliveryViewModel)GetValue(CustomerDeliveryItemProperty); }
            set { SetValue(CustomerDeliveryItemProperty, value); }
        }

        public static readonly BindableProperty ShowHeaderProperty = BindableProperty.Create(
            propertyName: "ShowHeader",
            returnType: typeof(bool),
            declaringType: typeof(DeliveryDetailView),
            defaultValue: true,
            defaultBindingMode: BindingMode.OneWay,
            propertyChanged: OnShowHeaderChanged
        );

        public bool ShowHeader
        {
            get { return (bool)GetValue(ShowHeaderProperty); }
            set { SetValue(ShowHeaderProperty, value); }
        }

        private static void OnShowHeaderChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bool)newValue)
            {
                ((DeliveryDetailView)bindable).ListViewHeader.HeightRequest = 0;
            }
        }

        public DeliveryDetailView()
        {
            InitializeComponent();
            BindingContext = this;
        }
    }
}