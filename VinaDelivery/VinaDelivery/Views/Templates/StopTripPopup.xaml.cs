﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.ViewModels.Templates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.Views.Templates
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StopTripPopup : PopupPage
	{
        private readonly StopTripPopupViewModel _viewModel;

        public StopTripPopup ()
		{
			InitializeComponent ();

            _viewModel = (StopTripPopupViewModel)BindingContext;
        }

        private void ReasonChanged(object sender, EventArgs e)
        {
            _viewModel.ReasonText = ((Editor)sender).Text;
        }
    }
}