﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Controls;
using VinaDelivery.Models;
using Acr.UserDialogs;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using Plugin.Iconize;
using Microsoft.AppCenter;

namespace VinaDelivery.Utils
{
    public static class TripUtil
    {
        public static async Task GetActiveTripAndSaveToStorageAsync(this ITripService tripService, IAppService appService)
        {
            try
            {
                App.Current.DeviceId = await AppCenter.GetInstallIdAsync();

                var trip = await tripService.GetActiveTripAsync();
                if (trip != null && (App.Current.Trip == null || trip.Id != App.Current.Trip.Id))
                {
                    App.Current.Trip = trip;
                    var appData = appService.AppDataStorage.GetAppData();
                    appData.CurrentTrip = App.Current.Trip;
                    appData.SyncDate = DateTime.Now.Date;
                    appData.FinishedStep = null;
                    appData.IsCurrentTripCompleted = false;
                    appService.AppDataStorage.SaveAppData(appData);

                    App.Current.MainPage = new RootPage { Detail = new IconNavigationPage(new TripPreparationPage()) };
                }
                else
                {
                    App.Current.MainPage = new RootPage() { Detail = new IconNavigationPage(new TripDonePage()) };
                }
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Phiên đăng nhập hết hạn, vui lòng đăng nhập lại", "Thông báo", "OK");
            }
        }

        public static void OrganizeCustomerDeliveries(this ObservableCollection<CustomerDeliveryViewModel> customerDeliveryItems)
        {
            var customerDictionary = new Dictionary<int, CustomerDeliveryViewModel>();
            var count = 1;
            foreach (var delivery in App.Current.Trip.Deliveries)
            {
                if (!customerDictionary.TryGetValue(delivery.Customer.Id, out var customerDelivery))
                {
                    customerDelivery = new CustomerDeliveryViewModel { Id = count++, Customer = delivery.Customer };
                    customerDeliveryItems.Add(customerDelivery);
                    customerDictionary.Add(delivery.Customer.Id, customerDelivery);
                }
                foreach (var item in delivery.Items)
                {
                    item.IsDone = false;
                    var pickedItem = App.Current.Trip.PickedDeliveryItems.FirstOrDefault(x => x.Product.Id == item.Product.Id);
                    if (pickedItem != null)
                    {
                        var pickupQuantity = pickedItem.PickupQuantity;
                        if (pickupQuantity > 0)
                        {
                            item.PickupQuantity = item.Quantity <= pickupQuantity
                                ? item.Quantity
                                : pickupQuantity;
                            pickupQuantity -= item.PickupQuantity;
                            item.IsDone = item.PickupQuantity == item.Quantity;
                        }
                    }
                }
                var groupingDeliveryItem = new GroupingList<DeliveryModel, DeliveryItemModel>(delivery, delivery.Items);
                customerDelivery.GroupingDeliveryItems.Add(groupingDeliveryItem);
                customerDelivery.IsPreparationDone = delivery.Items.All(i => i.IsDone);
            }
        }
    }
}
