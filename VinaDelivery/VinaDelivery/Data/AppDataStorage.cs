﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using SQLite;
using VinaDelivery.Models;
using Xamarin.Forms;

namespace VinaDelivery.Data
{
    public interface IAppDataStorage
    {
        AppData GetAppData();
        void SaveAppData(AppData appData);
    }

    public class AppDataStorage : IAppDataStorage
    {
        private static readonly object _locker = new object();
        private readonly SQLiteConnection _dbContext;

        public AppDataStorage()
        {
            var sqliteProvider = DependencyService.Get<ISQLiteProvider>();

            _dbContext = sqliteProvider.GetConnection("AppDb");
            _dbContext.CreateTable<AppData>();
        }

        public AppData GetAppData()
        {
            var appData = _dbContext.Table<AppData>().FirstOrDefault();
            if (appData == null)
            {
                appData = new AppData();
                _dbContext.Insert(appData);
            }

            return appData;
        }

        public void SaveAppData(AppData appData)
        {
            _dbContext.Update(appData);
        }
    }

    public class AppData
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string CurrentTripJson { get; set; }

        [Ignore]
        public TripModel CurrentTrip
        {
            get => CurrentTripJson == null ? null : JsonConvert.DeserializeObject<TripModel>(CurrentTripJson);
            set => CurrentTripJson = value == null ? null : JsonConvert.SerializeObject(value);
        }

        public int CurrentCustomerDeliveryId { get; set; }

        public DateTime SyncDate { get; set; }

        public string FinishedStep { get; set; }

        public bool IsCurrentTripCompleted { get; set; }

        public bool NeedSync { get; set; }
    }
}
