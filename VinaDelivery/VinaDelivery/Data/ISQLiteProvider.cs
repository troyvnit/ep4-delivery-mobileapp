﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace VinaDelivery.Data
{
    public interface ISQLiteProvider
    {
        /// <summary>
        /// Get SQlite connection
        /// </summary>
        /// <param name="databaseName">If database name contains "/", assume it is a full database path</param>
        /// <returns></returns>
        SQLiteConnection GetConnection(string databaseName);
    }
}
