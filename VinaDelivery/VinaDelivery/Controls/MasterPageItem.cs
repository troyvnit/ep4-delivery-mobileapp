﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VinaDelivery.Controls
{
    public class MasterPageItem
    {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Action Action { get; set; }
    }
}
