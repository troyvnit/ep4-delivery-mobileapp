﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace VinaDelivery.Controls
{
    /// <summary>
    /// For list view grouping
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="T"></typeparam>
    public class GroupingList<K, T> : ObservableCollection<T>
    {
        public K Key { get; }

        public GroupingList(K key, IEnumerable<T> items)
        {
            Key = key;
            foreach (var item in items)
                Items.Add(item);
        }
    }
}
