﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;

namespace VinaDelivery.Services
{
    public interface ISyncService
    {
        Task SyncTripAsync();
    }
}
