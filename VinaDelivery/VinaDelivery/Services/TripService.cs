﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Geolocator;
using VinaDelivery.Models;
using VinaDelivery.Services.Base;

namespace VinaDelivery.Services
{
    public class TripService : ITripService
    {
        private readonly IAuthRequestProvider _requestProvider;

        public TripService(IAuthRequestProvider requestProvider)
        {
            _requestProvider = requestProvider;
        }

        public async Task<TripModel> GetActiveTripAsync()
        {
            var trip = await _requestProvider.GetAsync<TripModel>($"{AppConfig.API_BASE_URL}/Trips/Current/{App.Current.DeviceId}");

            return trip;
        }

        public async Task<TripModel> ConfirmTripAsync(TripModel trip)
        {
            var tripRs = await _requestProvider.PostAsync<object, TripModel>($"{AppConfig.API_BASE_URL}/Trips/Confirm", 
                new {
                    TripId = trip.Id, DeliveryIds = trip.Deliveries.Where(x => x.Status.Id == (int)DeliveryStatusEnum.Picked).Select(x => x.Id),
                    //TripRowVersion = trip.RowVersionString
                });

            return tripRs;
        }

        public async Task<TripModel> StartTripAsync(TripModel trip)
        {
            var tripRs = await _requestProvider.PostAsync<object, TripModel>($"{AppConfig.API_BASE_URL}/Trips/Start",
                new
                {
                    TripId = trip.Id,
                    //TripRowVersion = trip.RowVersionString
                });

            return tripRs;
        }

        public async Task<TripModel> StopTripAsync(TripModel trip)
        {
            var tripRs = await _requestProvider.PostAsync<object, TripModel>($"{AppConfig.API_BASE_URL}/Trips/Stop", 
                new {
                    TripId = trip.Id, Reason = trip.StopReason,
                    //TripRowVersion = trip.RowVersionString
                });

            return tripRs;
        }

        public async Task<TripModel> CompleteTripAsync(TripModel trip)
        {
            var tripRs = await _requestProvider.PostAsync<object, TripModel>($"{AppConfig.API_BASE_URL}/Trips/Complete",
                new
                {
                    TripId = trip.Id,
                    //TripRowVersion = trip.RowVersionString
                });

            return tripRs;
        }

        public async Task StartDeliveryAsync(TripModel trip, IEnumerable<DeliveryModel> deliveries)
        {
            var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
            if (position == null)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi trong quá trình lấy vị trí hiện tại. Vui lòng thử lại.", "Thông báo");
                return;
            }

            await _requestProvider.PostAsync($"{AppConfig.API_BASE_URL}/Trips/StartDelivery",
                new
                {
                    TripId = trip.Id,
                    DeliveryIds = deliveries.Select(x => x.Id),
                    CurrentLatitude = (decimal)position.Latitude,
                    CurrentLongitude = (decimal)position.Longitude
                });
        }

        public async Task FinishDeliveryAsync(TripModel trip, IEnumerable<DeliveryModel> deliveries)
        {
            var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
            if (position == null)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi trong quá trình lấy vị trí hiện tại. Vui lòng thử lại.", "Thông báo");
                return;
            }

            await _requestProvider.PostAsync($"{AppConfig.API_BASE_URL}/Trips/FinishDelivery",
                new
                {
                    TripId = trip.Id,
                    Deliveries = deliveries.Select(x => new
                    {
                        x.Id,
                        x.Note,
                        x.SignatureFileName,
                        x.IsFailed,
                        Items = x.Items.Select(m => new
                        {
                            m.Id,
                            m.DeliveredQuantity
                        })
                    }),
                    CurrentLatitude = (decimal)position.Latitude,
                    CurrentLongitude = (decimal)position.Longitude
                });
        }

        public async Task UpdateTripLocationAsync(TripModel trip)
        {
            var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
            if (position == null)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi trong quá trình lấy vị trí hiện tại. Vui lòng thử lại.", "Thông báo");
                return;
            }

            await _requestProvider.PostAsync($"{AppConfig.API_BASE_URL}/Trips/UpdateTripLocation",
                new
                {
                    TripId = trip.Id,
                    CurrentLatitude = (decimal)position.Latitude,
                    CurrentLongitude = (decimal)position.Longitude
                });
        }

        public async Task<string> UploadSignatureAsync(Stream stream)
        {
            UserDialogs.Instance.ShowLoading();

            var result = await _requestProvider.UploadAsync<UploadSignatureResult>($"{AppConfig.API_BASE_URL}/Trips/UploadSignature", stream, "signature.png");

            UserDialogs.Instance.HideLoading();

            return result.FileName;
        }
    }
}
