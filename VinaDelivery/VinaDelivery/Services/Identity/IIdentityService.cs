﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VinaDelivery.Services.Identity
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> GetTokenAsync(string username, string password);
    }
}
