﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using JWT;
using JWT.Serializers;
using Newtonsoft.Json;

namespace VinaDelivery.Services.Identity
{
    public class IdentityService : IIdentityService
    {
        public async Task<AuthenticationResult> GetTokenAsync(string username, string password)
        {
            var tokenClient = new TokenClient($"{AppConfig.IDENTITY_PROVIDER}/connect/token", "mobileapp", "mobile");
            var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(username, password, "deliveryapi");

            if (tokenResponse.IsError)
                return null;

            var serializer = new JsonNetSerializer();
            var provider = new UtcDateTimeProvider();
            var validator = new JwtValidator(serializer, provider);
            var urlEncoder = new JwtBase64UrlEncoder();
            var decoder = new JwtDecoder(serializer, validator, urlEncoder);

            var json = decoder.Decode(tokenResponse.AccessToken);
            var temp = JsonConvert.DeserializeObject<dynamic>(json);

            var result = new AuthenticationResult
            {
                Username = temp.name,
                FullName = temp.FullName,
                AccessToken = tokenResponse.AccessToken,
                AccessTokenExpiresDate = DateTime.UtcNow.AddSeconds(tokenResponse.ExpiresIn).AddMinutes(-30)
            };

            return result;
        }
    }
}
