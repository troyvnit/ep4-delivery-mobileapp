﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VinaDelivery.Services.Identity
{
    public class AuthenticationResult
    {
        public string Username { get; set; }

        public string FullName { get; set; }

        public string AccessToken { get; set; }

        public DateTime AccessTokenExpiresDate { get; set; }
    }
}
