﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace VinaDelivery.Services.Base
{
    public interface IAuthRequestProvider
    {
        Task<TResult> GetAsync<TResult>(string uri);

        Task PostAsync(string uri, object data, string header = "");

        Task<TResult> PostAsync<TData, TResult>(string uri, TData data, string header = "");

        Task<TResult> PostAsync<TResult>(string uri, TResult data, string header = "");
        Task<TResult> UploadAsync<TResult>(string uri, Stream data, string fileName, string header = "");
    }
}
