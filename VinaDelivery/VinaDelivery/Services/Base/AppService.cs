﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;
using VinaDelivery.Data;

namespace VinaDelivery.Services.Base
{
    public class AppService : IAppService
    {
        public IAppDataStorage AppDataStorage { get; set; }

        public AppService(IAppDataStorage appDataStorage)
        {
            AppDataStorage = appDataStorage;
        }
    }
}
