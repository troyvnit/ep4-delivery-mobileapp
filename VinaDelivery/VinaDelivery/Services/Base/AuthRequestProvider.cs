﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VinaDelivery.Exceptions;
using VinaDelivery.Services.Identity;
using Xamarin.Forms;

namespace VinaDelivery.Services.Base
{
    public class AuthRequestProvider : IAuthRequestProvider
    {
        private readonly IIdentityService _identityService;
        private readonly IRequestProvider _requestProvider;

        private static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public AuthRequestProvider(IIdentityService identityService, IRequestProvider requestProvider)
        {
            _identityService = identityService;
            _requestProvider = requestProvider;
        }

        public async Task<TResult> GetAsync<TResult>(string uri)
        {
            try
            {
                await RenewTokenIfExpiredAsync();
                return await _requestProvider.GetAsync<TResult>(uri, Settings.AccessToken);
            }
            catch (ServiceAuthenticationException)
            {
                MessagingCenter.Send(this, "Logout");
            }

            return default(TResult);
        }

        public async Task PostAsync(string uri, object data, string header = "")
        {
            try
            {
                await RenewTokenIfExpiredAsync();
                await _requestProvider.PostAsync(uri, data, Settings.AccessToken, header);
            }
            catch (ServiceAuthenticationException)
            {
                MessagingCenter.Send(this, "Logout");
            }
        }

        public async Task<TResult> PostAsync<TData, TResult>(string uri, TData data, string header = "")
        {
            try
            {
                await RenewTokenIfExpiredAsync();
                return await _requestProvider.PostAsync<TData, TResult>(uri, data, Settings.AccessToken, header);
            }
            catch (ServiceAuthenticationException)
            {
                MessagingCenter.Send(this, "Logout");
            }

            return default(TResult);
        }

        public async Task<TResult> PostAsync<TResult>(string uri, TResult data, string header = "")
        {
            try
            {
                await RenewTokenIfExpiredAsync();
                return await _requestProvider.PostAsync<TResult>(uri, data, Settings.AccessToken, header);
            }
            catch (ServiceAuthenticationException)
            {
                MessagingCenter.Send(this, "Logout");
            }

            return default(TResult);
        }

        public async Task<TResult> UploadAsync<TResult>(string uri, Stream data, string fileName, string header = "")
        {
            try
            {
                await RenewTokenIfExpiredAsync();
                return await _requestProvider.UploadAsync<TResult>(uri, data, Settings.AccessToken, header);
            }
            catch (ServiceAuthenticationException)
            {
                MessagingCenter.Send(this, "Logout");
            }

            return default(TResult);
        }

        private async Task RenewTokenIfExpiredAsync()
        {
            await _semaphore.WaitAsync();

            try
            {
                if (Settings.AccessTokenExpiresDate <= DateTime.UtcNow)
                {
                    var result = await _identityService.GetTokenAsync(Settings.Username, Settings.Password);
                    if (result == null)
                    {
                        throw new ServiceAuthenticationException();
                    }
                    else
                    {
                        Settings.AccessToken = result.AccessToken;
                        Settings.AccessTokenExpiresDate = result.AccessTokenExpiresDate;
                    }
                }
            }
            finally
            {
                //When the task is ready, release the semaphore. It is vital to ALWAYS release the semaphore when we are ready, or else we will end up with a Semaphore that is forever locked.
                //This is why it is important to do the Release within a try...finally clause; program execution may crash or take a different path, this way you are guaranteed execution
                _semaphore.Release();
            }
        }
    }
}
