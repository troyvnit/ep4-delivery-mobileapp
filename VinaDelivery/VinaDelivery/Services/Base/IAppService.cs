﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;
using VinaDelivery.Data;

namespace VinaDelivery.Services.Base
{
    public interface IAppService
    {
        IAppDataStorage AppDataStorage { get; set; }
    }
}
