﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AppCenter;
using Prism.Services;
using VinaDelivery.Data;
using VinaDelivery.Models;
using VinaDelivery.Providers;

namespace VinaDelivery.Services
{
    public class SyncService : ISyncService
    {
        private readonly IAppDataStorage _appDataStorage;
        private readonly IStorageProvider _storageProvider;
        private readonly ITripService _tripService;

        public SyncService(IAppDataStorage appDataStorage, ITripService tripService)
        {
            _appDataStorage = appDataStorage;
            _storageProvider = Xamarin.Forms.DependencyService.Get<IStorageProvider>();
            _tripService = tripService;
        }

        public async Task SyncTripAsync()
        {
            var trip = App.Current.Trip;
            if (trip == null)
                return;

            await _tripService.UpdateTripLocationAsync(trip);

            var appData = _appDataStorage.GetAppData();
            if (!appData.NeedSync)
                return;

            // sync deliveries
            var finishedDeliveries = trip.Deliveries.Where(x => x.StatusEnum == DeliveryStatusEnum.Done || x.StatusEnum == DeliveryStatusEnum.PartialDone || x.StatusEnum == DeliveryStatusEnum.Failed).ToList();
            var finishedDeliveriesByCustomer = finishedDeliveries.GroupBy(x => x.Customer.Id).ToList();
            foreach (var deliveryGroup in finishedDeliveriesByCustomer)
            {
                var deliveries = deliveryGroup.ToList();
                if (!deliveries.Any())
                    continue;

                // upload signature
                var firstDeliveryWithSignature = deliveries.FirstOrDefault(x => !string.IsNullOrEmpty(x.SignatureFileName));
                if (firstDeliveryWithSignature != null)
                {
                    try
                    {
                        var filePath = deliveries.FirstOrDefault(x => !string.IsNullOrEmpty(x.SignatureFileName)).SignatureFileName;
                        var fileBytes = _storageProvider.ReadFile(filePath);

                        var fileName = await _tripService.UploadSignatureAsync(new MemoryStream(fileBytes));
                        deliveries.ForEach(x => x.SignatureFileName = fileName);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            if (finishedDeliveriesByCustomer.Any())
            {
                await _tripService.FinishDeliveryAsync(App.Current.Trip, finishedDeliveries);
            }

            var onDeliveryDeliveries = trip.Deliveries.Where(x => x.StatusEnum == DeliveryStatusEnum.OnDelivery).ToList();
            if (onDeliveryDeliveries.Any())
            {
                await _tripService.StartDeliveryAsync(App.Current.Trip, onDeliveryDeliveries);
            }

            // sync trip
            if (trip.StatusEnum == TripStatusEnum.OnTrip)
            {
                await _tripService.StartTripAsync(trip);
                trip.RowVersionString = trip.RowVersionString;
            }
            else if (trip.StatusEnum == TripStatusEnum.Stop)
            {
                await _tripService.StopTripAsync(trip);
                trip.RowVersionString = trip.RowVersionString;
            }
            else if (trip.StatusEnum == TripStatusEnum.Completed)
            {
                await _tripService.CompleteTripAsync(trip);
                trip.RowVersionString = trip.RowVersionString;
                appData.IsCurrentTripCompleted = true;
            }

            appData.NeedSync = false;
            appData.CurrentTrip = trip;
            _appDataStorage.SaveAppData(appData);
        }
    }
}
