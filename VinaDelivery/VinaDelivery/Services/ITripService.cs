﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;

namespace VinaDelivery.Services
{
    public interface ITripService
    {
        Task<TripModel> GetActiveTripAsync();

        Task<TripModel> ConfirmTripAsync(TripModel trip);

        Task<TripModel> StartTripAsync(TripModel trip);

        Task<TripModel> StopTripAsync(TripModel trip);

        Task<TripModel> CompleteTripAsync(TripModel trip);

        Task UpdateTripLocationAsync(TripModel trip);

        Task<string> UploadSignatureAsync(Stream stream);

        Task StartDeliveryAsync(TripModel trip, IEnumerable<DeliveryModel> deliveries);

        Task FinishDeliveryAsync(TripModel trip, IEnumerable<DeliveryModel> deliveries);
    }
}
