﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using VinaDelivery.Services.Base;
using Xamarin.Forms;

namespace VinaDelivery.ViewModels.Base
{
    public abstract class ViewModelBase : BindableBase, INavigationAware
    {
        protected readonly IAppService AppService;

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        public DelegateCommand<string> NavigateCommand { get; set; }
        public DelegateCommand<string> OpenUrlCommand { get; set; }

        protected ViewModelBase(IAppService appService)
        {
            AppService = appService;

            NavigateCommand = new DelegateCommand<string>(async (s) => await NavigateAsync(s));
            OpenUrlCommand = new DelegateCommand<string>(OpenUrlAsync);
        }

        private async Task NavigateAsync(string target)
        {
            //await AppService.Navigation.NavigateAsync(target);
        }

        private void OpenUrlAsync(string url)
        {
            Uri uri = new Uri(url);
            Device.OpenUri(uri);
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            
        }
    }
}
