﻿using Acr.UserDialogs;
using Prism.Commands;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using VinaDelivery.Controls;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Utils;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Templates;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using VinaDelivery.ViewModels.Templates;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripTrackingDeliveryPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private CustomerDeliveryViewModel _delivery;

        public CustomerDeliveryViewModel Delivery
        {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        private bool _isMapTabVisibile = true;
        public bool IsMapTabVisibile
        {
            get => _isMapTabVisibile;
            set => SetProperty(ref _isMapTabVisibile, value);
        }

        private bool _isDetailTabVisibile;
        public bool IsDetailTabVisibile
        {
            get => _isDetailTabVisibile;
            set => SetProperty(ref _isDetailTabVisibile, value);
        }

        public DelegateCommand SwitchTabCommand { get; set; }
        public DelegateCommand DeliveryCommand { get; set; }
        public DelegateCommand StopCommand { get; set; }

        public TripTrackingDeliveryPageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            SwitchTabCommand = new DelegateCommand(ExecuteSwitchTabCommand);
            DeliveryCommand = new DelegateCommand(async () => await ExecuteDeliveryCommandAsync());
            StopCommand = new DelegateCommand(async () => await ExecuteStopCommandAsync());
        }

        private async Task ExecuteDeliveryCommandAsync()
        {
            try
            {
                var deliveries = Delivery.GroupingDeliveryItems.Select(x => x.Key).ToList();

                deliveries.ForEach(x => x.Status = new DeliveryStatus { Id = (int)DeliveryStatusEnum.OnDelivery });

                var appData = AppService.AppDataStorage.GetAppData();
                appData.NeedSync = true;
                appData.CurrentTrip = App.Current.Trip;
                AppService.AppDataStorage.SaveAppData(appData);

                // trigger background service to sync trip
                MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripDeliveryPage(_delivery));
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        private async Task ExecuteStopCommandAsync()
        {
            await UserDialogs.Instance.AlertAsync("Vui lòng ghi chú lý do dừng giao hàng ở đây!");
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new NoteDeliveryPopup(_delivery, NoteFor.StopDelivery));
        }

        private void ExecuteSwitchTabCommand()
        {
            IsDetailTabVisibile = !IsDetailTabVisibile;
            IsMapTabVisibile = !IsMapTabVisibile;
        }

        public void LoadData(CustomerDeliveryViewModel delivery)
        {
            Delivery = delivery;
        }
    }
}
