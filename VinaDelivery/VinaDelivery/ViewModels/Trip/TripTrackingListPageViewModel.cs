﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using VinaDelivery.Models;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using System.Linq;
using Acr.UserDialogs;
using Plugin.Geolocator;
using VinaDelivery.Services;
using VinaDelivery.Utils;
using Xamarin.Forms;
using Rg.Plugins.Popup.Extensions;
using VinaDelivery.Views.Templates;
using Plugin.Iconize;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripTrackingListPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;

        private int _totalPickup;
        public int TotalPickup {
            get => _totalPickup;
            set => SetProperty(ref _totalPickup, value);
        }

        private int _totalDelivered;
        public int TotalDelivered {
            get => _totalDelivered;
            set => SetProperty(ref _totalDelivered, value);
        }

        private int _totalRemaining;
        public int TotalRemaining {
            get => _totalRemaining;
            set => SetProperty(ref _totalRemaining, value);
        }

        public ObservableCollection<CustomerDeliveryViewModel> CustomerDeliveryItems { get; set; } = new ObservableCollection<CustomerDeliveryViewModel>();

        public DelegateCommand<CustomerDeliveryViewModel> OpenDetailCommand { get; set; }
        public DelegateCommand<CustomerDeliveryViewModel> StartCommand { get; set; }
        public DelegateCommand StopCommand { get; set; }
        public DelegateCommand CompleteCommand { get; set; }

        public TripTrackingListPageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            OpenDetailCommand = new DelegateCommand<CustomerDeliveryViewModel>(async (x) => await ExecuteOpenDetailCommandAsync(x));
            StartCommand = new DelegateCommand<CustomerDeliveryViewModel>(async (x) => await ExecuteStartCommandAsync(x));
            StopCommand = new DelegateCommand(async () => await ExecuteStopCommandAsync());
            CompleteCommand = new DelegateCommand(async () => await ExecuteCompleteCommandAsync());
        }

        private async Task ExecuteOpenDetailCommandAsync(CustomerDeliveryViewModel delivery)
        {
            if (delivery.Status == CustomerDeliveryStatusEnum.Pending)
            {
                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripTrackingDetailPage(delivery));
            }
            else if (delivery.Status == CustomerDeliveryStatusEnum.Started)
            {
                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripTrackingDeliveryPage(delivery));
            }
        }

        private async Task ExecuteStartCommandAsync(CustomerDeliveryViewModel delivery)
        {
            try
            {
                UserDialogs.Instance.ShowLoading();

                //var trip = await _tripService.StartTripAsync(App.Current.Trip);
                //App.Current.Trip.Status = trip.Status;
                //App.Current.Trip.RowVersionString = trip.RowVersionString;
                App.Current.Trip.Status = new TripStatus { Id = (int)TripStatusEnum.OnTrip };

                var deliveries = delivery.GroupingDeliveryItems.Select(x => x.Key).ToList();
                deliveries.ForEach(x => x.Status = new DeliveryStatus { Id = (int)DeliveryStatusEnum.OnDelivery });

                var appData = AppService.AppDataStorage.GetAppData();
                appData.NeedSync = true;
                appData.CurrentTrip = App.Current.Trip;
                appData.CurrentCustomerDeliveryId = delivery.Id;
                AppService.AppDataStorage.SaveAppData(appData);

                // trigger background service to sync trip
                MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

                //if (!CrossGeolocator.Current.IsListening)
                //{
                //    await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromMinutes(5), 0);

                //    CrossGeolocator.Current.PositionChanged += (sender, arg) =>
                //    {
                //        Device.BeginInvokeOnMainThread(async () =>
                //        {
                //            try
                //            {
                //                await _tripService.UpdateTripLocationAsync(trip);
                //            }
                //            catch (Exception e)
                //            {
                //            }
                //        });
                //    };
                //}

                UserDialogs.Instance.HideLoading();

                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripTrackingDeliveryPage(delivery));
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        private async Task ExecuteStopCommandAsync()
        {
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new StopTripPopup());
        }

        private async Task ExecuteCompleteCommandAsync()
        {
            try
            {
                App.Current.Trip.Status = new TripStatus { Id = (int)TripStatusEnum.Completed };

                var appData = AppService.AppDataStorage.GetAppData();
                appData.IsCurrentTripCompleted = true;
                appData.NeedSync = true;
                appData.CurrentTrip = App.Current.Trip;
                AppService.AppDataStorage.SaveAppData(appData);

                // trigger background service to sync trip
                MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

                await UserDialogs.Instance.AlertAsync($"Bạn đã hoàn tất chuyến {App.Current.Trip.Code}!", "Chúc mừng!");

                App.Current.MainPage = new RootPage() { Detail = new IconNavigationPage(new TripDonePage()) };
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        public void LoadData()
        {
            CustomerDeliveryItems.Clear();

            if (App.Current.Trip != null)
            {
                if(!App.Current.Trip.CustomerDeliveryItems.Any())
                {
                    App.Current.Trip.CustomerDeliveryItems.OrganizeCustomerDeliveries();
                }

                foreach(var item in App.Current.Trip.CustomerDeliveryItems)
                {
                    if (item.IsPreparationDone)
                    {
                        CustomerDeliveryItems.Add(item);
                    }
                }

                TotalPickup = CustomerDeliveryItems.Sum(x => x.GroupingDeliveryItems.Count(y => y.Key.Status.Id == (int)DeliveryStatusEnum.Picked || y.Key.Status.Id == (int)DeliveryStatusEnum.Done || y.Key.Status.Id == (int)DeliveryStatusEnum.PartialDone));
                TotalDelivered = CustomerDeliveryItems.Sum(x => x.GroupingDeliveryItems.Count(y => y.Key.Status.Id == (int)DeliveryStatusEnum.Done || y.Key.Status.Id == (int)DeliveryStatusEnum.PartialDone));
                TotalRemaining = TotalPickup - TotalDelivered;
            }
        }
    }
}
