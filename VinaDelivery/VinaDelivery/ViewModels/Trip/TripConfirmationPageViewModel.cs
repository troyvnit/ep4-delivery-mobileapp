﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using VinaDelivery.Controls;
using VinaDelivery.Models;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using Acr.UserDialogs;
using Plugin.Iconize;
using VinaDelivery.Data;
using VinaDelivery.Services;
using VinaDelivery.Utils;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripConfirmationPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        public int TripId => App.Current.Trip.Id;

        public string TripCode => App.Current.Trip.Code;

        private int _carouselPosition;
        public int CarouselPosition
        {
            get => _carouselPosition;
            set => SetProperty(ref _carouselPosition, value);
        }

        private int _totalDone;
        public int TotalDone
        {
            get => _totalDone;
            set => SetProperty(ref _totalDone, value);
        }

        private int _totalPending;
        public int TotalPending
        {
            get => _totalPending;
            set => SetProperty(ref _totalPending, value);
        }

        public ObservableCollection<CustomerDeliveryViewModel> CustomerDeliveryItems { get; set; } = new ObservableCollection<CustomerDeliveryViewModel>();

        private CustomerDeliveryViewModel _currentCustomerDeliveryItem;
        public CustomerDeliveryViewModel CurrentCustomerDeliveryItem {
            get => _currentCustomerDeliveryItem;
            set => SetProperty(ref _currentCustomerDeliveryItem, value);
        }

        public DelegateCommand JumpToDoneItemCommand { get; set; }
        public DelegateCommand JumpToPendingItemCommand { get; set; }
        public DelegateCommand PreviousItemCommand { get; set; }
        public DelegateCommand NextItemCommand { get; set; }
        public DelegateCommand ConfirmCommand { get; set; }

        public TripConfirmationPageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            JumpToDoneItemCommand = new DelegateCommand(ExecuteJumpToDoneItemCommand);
            JumpToPendingItemCommand = new DelegateCommand(ExecuteJumpToPendingItemCommand);
            PreviousItemCommand = new DelegateCommand(ExecutePreviousItemCommand);
            NextItemCommand = new DelegateCommand(ExecuteNextItemCommand);
            ConfirmCommand = new DelegateCommand(async () => await ExecuteConfirmCommandAsync());
        }

        private async Task ExecuteConfirmCommandAsync()
        {
            bool goToTripTracking = false;
            if(CustomerDeliveryItems.All(x => x.IsPreparationDone))
            {
                if (await UserDialogs.Instance.ConfirmAsync("Bạn chắc chắn đã nhận đủ hàng để giao?", "Thông báo", "OK", "Cancel"))
                {
                    goToTripTracking = true;
                }
            }
            else
            {
                if (await UserDialogs.Instance.ConfirmAsync("Tồn tại đơn hàng chưa hoàn tất nhận hàng. Chuyển sang danh sách chờ?", "Thông báo", "Yes", "No"))
                {
                    goToTripTracking = true;
                }
            }

            if (goToTripTracking)
            {
                UserDialogs.Instance.ShowLoading();

                App.Current.Trip.CustomerDeliveryItems = CustomerDeliveryItems;
                var deliveries = new List<DeliveryModel>();
                foreach (var cdi in CustomerDeliveryItems)
                {
                    var items = cdi.GroupingDeliveryItems.Select(x => x.Key);
                    foreach (var delivery in items)
                    {
                        delivery.Status = delivery.Items.All(x => x.IsDone) ? new DeliveryStatus((int)DeliveryStatusEnum.Picked, DeliveryStatusEnum.Picked.ToString()) : new DeliveryStatus((int)DeliveryStatusEnum.Pending, DeliveryStatusEnum.Pending.ToString());
                        deliveries.Add(delivery);
                    }
                }
                App.Current.Trip.Deliveries = deliveries;

                var trip = await _tripService.ConfirmTripAsync(App.Current.Trip);
                App.Current.Trip.Status = trip.Status;
                App.Current.Trip.RowVersionString = trip.RowVersionString;

                var appData = AppService.AppDataStorage.GetAppData();
                appData.FinishedStep = "Confirmation";
                appData.CurrentTrip = App.Current.Trip;
                AppService.AppDataStorage.SaveAppData(appData);

                UserDialogs.Instance.HideLoading();

                //await ((RootPage)App.Current.MainPage).Detail.Navigation.PushAsync(new TripTrackingPage());

                ((RootPage)App.Current.MainPage).Detail = new IconNavigationPage(new TripTrackingPage());
            }
        }

        private void ExecuteJumpToPendingItemCommand()
        {
            if(CustomerDeliveryItems.Any(x => !x.IsPreparationDone))
            {
                CurrentCustomerDeliveryItem = CustomerDeliveryItems.FirstOrDefault(x => !x.IsPreparationDone);
            }
        }

        private void ExecuteJumpToDoneItemCommand()
        {
            if(CustomerDeliveryItems.Any(x => x.IsPreparationDone))
            {
                CurrentCustomerDeliveryItem = CustomerDeliveryItems.FirstOrDefault(x => x.IsPreparationDone);
            }
        }

        private void ExecutePreviousItemCommand()
        {
            var position = CustomerDeliveryItems.IndexOf(CurrentCustomerDeliveryItem);
            if(position == 0)
            {
                position = CustomerDeliveryItems.Count - 1;
            }
            else
            {
                position--;
            }
            CurrentCustomerDeliveryItem = CustomerDeliveryItems.ElementAtOrDefault(position);
        }

        private void ExecuteNextItemCommand()
        {
            var position = CustomerDeliveryItems.IndexOf(CurrentCustomerDeliveryItem);
            if (position == CustomerDeliveryItems.Count - 1)
            {
                position = 0;
            }
            else
            {
                position++;
            }
            CurrentCustomerDeliveryItem = CustomerDeliveryItems.ElementAtOrDefault(position);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            LoadData();
        }

        public void LoadData()
        {
            if (App.Current.Trip != null)
            {
                CustomerDeliveryItems.Clear();

                CustomerDeliveryItems.OrganizeCustomerDeliveries();

                TotalDone = CustomerDeliveryItems.Count(x => x.IsPreparationDone);
                TotalPending = CustomerDeliveryItems.Count(x => !x.IsPreparationDone);

                CurrentCustomerDeliveryItem = CustomerDeliveryItems.FirstOrDefault();
            }
        }

        public class CustomerDeliveryViewModel
        {
            public int Id { get; set; }

            public bool IsPreparationDone { get; set; }

            public int TotalItemDone => GroupingDeliveryItems.Count(x => x.All(y => y.IsDeliveryDone));

            public int TotalItemPending => GroupingDeliveryItems.Count(x => x.All(y => !y.IsDeliveryDone));

            public bool IsDeliveryDone { get; set; }

            public int TotalDeliveryItemDone { get; set; }

            public int TotalDeliveryItemPending { get; set; }

            public decimal Distance { get; set; }

            public decimal Time { get; set; }

            public string TotalItemDisplay => $"{TotalItemDone}/{TotalItemPending}";

            public CustomerModel Customer { get; set; }

            public CustomerDeliveryStatusEnum Status
            {
                get
                {
                    if (GroupingDeliveryItems.All(x => x.Key.StatusEnum == DeliveryStatusEnum.Picked))
                    {
                        return CustomerDeliveryStatusEnum.Pending;
                    }

                    if (GroupingDeliveryItems.All(x => x.Key.StatusEnum == DeliveryStatusEnum.Done))
                    {
                        return CustomerDeliveryStatusEnum.Done;
                    }

                    if (GroupingDeliveryItems.All(x => x.Key.StatusEnum == DeliveryStatusEnum.Failed))
                    {
                        return CustomerDeliveryStatusEnum.Failed;
                    }

                    if (GroupingDeliveryItems.All(x => x.Key.StatusEnum == DeliveryStatusEnum.PartialDone)
                        || GroupingDeliveryItems.Any(x => x.Key.StatusEnum == DeliveryStatusEnum.Done) && (GroupingDeliveryItems.Any(x => x.Key.StatusEnum == DeliveryStatusEnum.PartialDone) || GroupingDeliveryItems.Any(x => x.Key.StatusEnum == DeliveryStatusEnum.Failed)))
                    {
                        return CustomerDeliveryStatusEnum.PartialDone;
                    }

                    return CustomerDeliveryStatusEnum.Started;
                }
            }

            public string Note { get; set; }

            public ObservableCollection<GroupingList<DeliveryModel, DeliveryItemModel>> GroupingDeliveryItems { get; set; } = new ObservableCollection<GroupingList<DeliveryModel, DeliveryItemModel>>();

            public bool IsDeliveryDoneAll => GroupingDeliveryItems.All(x => x.All(y => y.IsDeliveryDone));

            public bool IsFullDelivered => GroupingDeliveryItems.All(x => x.All(y => y.IsFullDelivered));
        }
    }
}
