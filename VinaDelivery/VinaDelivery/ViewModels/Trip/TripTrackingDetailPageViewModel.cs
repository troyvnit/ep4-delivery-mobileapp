﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Geolocator;
using Prism.Commands;
using VinaDelivery.Controls;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripTrackingDetailPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private CustomerDeliveryViewModel _delivery;
        public CustomerDeliveryViewModel Delivery {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        public DelegateCommand GoToDeliveryPageCommand { get; set; }

        public TripTrackingDetailPageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;

            GoToDeliveryPageCommand = new DelegateCommand(async() => await ExecuteGoToDeliveryPageCommandAsync());
        }

        private async Task ExecuteGoToDeliveryPageCommandAsync()
        {
            try
            {
                UserDialogs.Instance.ShowLoading();

                var trip = await _tripService.StartTripAsync(App.Current.Trip);
                App.Current.Trip.Status = trip.Status;
                App.Current.Trip.RowVersionString = trip.RowVersionString;

                var appData = AppService.AppDataStorage.GetAppData();
                appData.CurrentTrip = App.Current.Trip;
                appData.CurrentCustomerDeliveryId = Delivery.Id;
                AppService.AppDataStorage.SaveAppData(appData);

                if (!CrossGeolocator.Current.IsListening)
                {
                    await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromMinutes(5), 0);

                    CrossGeolocator.Current.PositionChanged += (sender, arg) =>
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            try
                            {
                                await _tripService.UpdateTripLocationAsync(trip);
                            }
                            catch (Exception e)
                            {
                            }
                        });
                    };
                }

                UserDialogs.Instance.HideLoading();

                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushModalAsync(new TripTrackingDeliveryPage(Delivery));
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        public void LoadData(CustomerDeliveryViewModel delivery)
        {
            Delivery = delivery;
        }
    }
}
