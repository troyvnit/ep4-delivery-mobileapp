﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using VinaDelivery.Models;
using System.Threading.Tasks;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Utils;
using Prism.Commands;
using Acr.UserDialogs;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripDonePageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;

        private string _message;
        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);
        }

        public DelegateCommand ReloadCommand { get; set; }
        public TripDonePageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            ReloadCommand = new DelegateCommand(async () => await ExecuteReloadCommandAsync());
        }

        private async Task ExecuteReloadCommandAsync()
        {
            UserDialogs.Instance.ShowLoading();

            await _tripService.GetActiveTripAndSaveToStorageAsync(AppService);

            UserDialogs.Instance.HideLoading();
        }
    }
}
