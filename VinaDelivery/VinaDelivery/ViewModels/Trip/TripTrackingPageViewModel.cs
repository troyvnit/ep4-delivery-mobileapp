﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using VinaDelivery.Models;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripTrackingPageViewModel : ViewModelBase
    {
        public string TripCode => App.Current.Trip.Code;

        public TripTrackingPageViewModel(IAppService appService) : base(appService)
        {
        }
    }
}
