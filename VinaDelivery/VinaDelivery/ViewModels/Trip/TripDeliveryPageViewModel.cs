﻿using Acr.UserDialogs;
using Prism.Commands;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Controls;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.ViewModels.Templates;
using VinaDelivery.Views;
using VinaDelivery.Views.Templates;
using VinaDelivery.Views.Trip;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripDeliveryPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private CustomerDeliveryViewModel _delivery;

        public CustomerDeliveryViewModel Delivery
        {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        private bool _isMapTabVisibile = true;
        public bool IsMapTabVisibile
        {
            get => _isMapTabVisibile;
            set => SetProperty(ref _isMapTabVisibile, value);
        }

        private bool _isDetailTabVisibile;
        public bool IsDetailTabVisibile
        {
            get => _isDetailTabVisibile;
            set => SetProperty(ref _isDetailTabVisibile, value);
        }

        public DelegateCommand SwitchTabCommand { get; set; }
        public DelegateCommand NoteCommand { get; set; }
        public DelegateCommand CompleteCommand { get; set; }
        public DelegateCommand<DeliveryItemModel> DoneCommand { get; set; }

        public TripDeliveryPageViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            SwitchTabCommand = new DelegateCommand(ExecuteSwitchTabCommand);
            NoteCommand = new DelegateCommand(async () => await ExecuteNoteCommandAsync());
            CompleteCommand = new DelegateCommand(async () => await ExecuteCompleteCommandAsync());
            DoneCommand = new DelegateCommand<DeliveryItemModel>(async x => await ExecuteDoneCommand(x));
        }

        private async Task ExecuteNoteCommandAsync()
        {
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new NoteDeliveryPopup(_delivery));
        }

        private async Task ExecuteCompleteCommandAsync()
        {
            if (!Delivery.GroupingDeliveryItems.Any(x => x.Any(y => y.IsDeliveryDone)))
            {
                await UserDialogs.Instance.AlertAsync("Vui lòng giao ít nhất 1 item!");
                return;
            }

            if (Delivery.GroupingDeliveryItems.Any(x => x.Any(y => y.IsDeliveryDone && (y.DeliveredQuantity > y.Quantity || y.DeliveredQuantity > y.Quantity))))
            {
                await UserDialogs.Instance.AlertAsync("Số lượng hàng giao vượt quá số lượng hàng cần giao!", "Lỗi");
                return;
            }

            if (Delivery.GroupingDeliveryItems.Any(x => x.Any(y => y.IsDeliveryDone && (y.DeliveredQuantity <= 0))))
            {
                await UserDialogs.Instance.AlertAsync("Số lượng hàng giao phải lớn hơn 0!", "Lỗi");
                return;
            }

            var deliveries = Delivery.GroupingDeliveryItems.Select(x => x.Key).Where(x => x.Items.Any(y => y.IsDeliveryDone));
            //var message = "Xác nhận giao hàng:";
            //foreach(var delivery in deliveries)
            //{
            //    message += $" - Đơn {delivery.Code}: ";
            //    message += delivery.Items.All(x => x.IsFullDelivered) ? "Giao đủ" : "Giao một phần";
            //}

            //if (await UserDialogs.Instance.ConfirmAsync(message, "Thông báo", "OK", "Cancel"))
            //{
            //    if (!Delivery.IsFullDelivered && string.IsNullOrEmpty(Delivery.Note))
            //    {
            //        await UserDialogs.Instance.AlertAsync("Vui lòng ghi chú cho đơn hàng chỉ giao một phần!");
            //        await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new NoteDeliveryPopup(_delivery, NoteFor.CompleteDelivery));
            //    }
            //    else
            //    {
            //        await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new CompleteDeliveryPopup(_delivery));
            //    }
            //}

            if (!Delivery.IsFullDelivered && string.IsNullOrEmpty(Delivery.Note))
            {
                await UserDialogs.Instance.AlertAsync("Vui lòng ghi chú cho đơn hàng chỉ giao một phần!");
                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new NoteDeliveryPopup(_delivery, NoteFor.CompleteDelivery));
            }
            else
            {
                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new CompleteDeliveryPopup(_delivery));
            }
        }

        private async Task ExecuteDoneCommand(DeliveryItemModel item)
        {
            if (item.DeliveredQuantity > item.Quantity)
            {
                await UserDialogs.Instance.AlertAsync("Số lượng hàng giao vượt quá số lượng hàng cần giao!", "Lỗi");
                return;
            }

            if (item.DeliveredQuantity > item.PickupQuantity)
            {
                await UserDialogs.Instance.AlertAsync("Số lượng hàng giao vượt quá số lượng hàng đã nhận!", "Lỗi");
                return;
            }

            if (item.DeliveredQuantity <= 0)
            {
                await UserDialogs.Instance.AlertAsync("Số lượng hàng giao phải lớn hơn 0!", "Lỗi");
                return;
            }

            item.DeliveredQuantity = item.DeliveredQuantity == 0 ? item.Quantity : item.DeliveredQuantity;
            item.IsDeliveryDone = !item.IsDeliveryDone;
        }

        private void ExecuteSwitchTabCommand()
        {
            IsDetailTabVisibile = !IsDetailTabVisibile;
            IsMapTabVisibile = !IsMapTabVisibile;
        }

        public void LoadData(CustomerDeliveryViewModel delivery)
        {
            Delivery = delivery;
        }
    }
}
