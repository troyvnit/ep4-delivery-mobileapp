﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using VinaDelivery.Models;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using Acr.UserDialogs;
using Plugin.Iconize;
using VinaDelivery.Data;

namespace VinaDelivery.ViewModels.Trip
{
    public class TripPreparationPageViewModel : ViewModelBase
    {
        private bool _isTripPreparationDone;
        public bool IsTripPreparationDone
        {
            get => _isTripPreparationDone;
            set => SetProperty(ref _isTripPreparationDone, value);
        }

        public int TripId => App.Current.Trip.Id;

        public string TripCode => App.Current.Trip.Code;

        public ObservableCollection<DeliveryItemModel> DeliveryItems { get; set; } = new ObservableCollection<DeliveryItemModel>();

        public DelegateCommand<DeliveryItemModel> ConfirmDeliveryItemCommand { get; set; }
        public DelegateCommand DonePreparationCommand { get; set; }

        public TripPreparationPageViewModel(IAppService appService) : base(appService)
        {
            ConfirmDeliveryItemCommand = new DelegateCommand<DeliveryItemModel>(ExecuteConfirmDeliveryItemCommand);
            DonePreparationCommand = new DelegateCommand(async () => await ExecuteDonePreparationCommandAsync());
        }

        private async Task ExecuteDonePreparationCommandAsync()
        {
            if (DeliveryItems.All(x => !x.IsDone))
            {
                await UserDialogs.Instance.AlertAsync("Vui lòng nhận hàng trước khi giao.", "Thông báo");
            }
            else
            {
                App.Current.Trip.PickedDeliveryItems.Clear();
                foreach(var deliveryItem in DeliveryItems.Where(x => x.IsDone))
                {
                    App.Current.Trip.PickedDeliveryItems.Add(deliveryItem);
                }

                var appData = AppService.AppDataStorage.GetAppData();
                appData.CurrentTrip = App.Current.Trip;
                AppService.AppDataStorage.SaveAppData(appData);

                await ((RootPage)App.Current.MainPage).Detail.Navigation.PushAsync(new TripConfirmationPage());
            }
        }

        private void ExecuteConfirmDeliveryItemCommand(DeliveryItemModel deliveryItem)
        {
            deliveryItem.IsDone = !deliveryItem.IsDone;
            IsTripPreparationDone = DeliveryItems.All(x => x.IsDone);
            if (deliveryItem.IsDone)
            {
                deliveryItem.PickupQuantity = deliveryItem.Quantity;
            }
            else
            {
                deliveryItem.PickupQuantity = 0;
            }
        }

        public void LoadData()
        {
            DeliveryItems.Clear();

            if (App.Current.Trip != null)
            {
                var deliveryItems = new List<DeliveryItemModel>();
                foreach (var delivery in App.Current.Trip.Deliveries)
                {
                    foreach (var item in delivery.Items)
                    {
                        item.Deliveries = $"{delivery.Code}";
                        deliveryItems.Add(item);
                    }
                }

                var groupedDeliveryItems = deliveryItems
                        .GroupBy(x => new { ProductId = x.Product.Id, Lot = x.Lot, LotDate = x.LotDate }).Select(g => new DeliveryItemModel()
                        {
                            Id = g.FirstOrDefault().Id,
                            Product = g.FirstOrDefault().Product,
                            Lot = g.Key.Lot,
                            LotDate = g.Key.LotDate,
                            DeliveredQuantity = g.Sum(x => x.DeliveredQuantity),
                            PickupQuantity = g.Sum(x => x.PickupQuantity),
                            Quantity = g.Sum(x => x.Quantity),
                            Deliveries = string.Join(", ", g.Select(x => x.Deliveries).Distinct()),
                            WarehouseCode = g.FirstOrDefault().WarehouseCode,
                            WarehouseName = g.FirstOrDefault().WarehouseName,
                            IsDone = g.FirstOrDefault().IsDone
                        });

                foreach (var item in groupedDeliveryItems)
                {
                    DeliveryItems.Add(item);
                }

                IsTripPreparationDone = DeliveryItems.All(x => x.IsDone);
            }
        }
    }
}
