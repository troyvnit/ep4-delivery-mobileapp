﻿using Acr.UserDialogs;
using Prism.Commands;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Plugin.Iconize;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;
using Plugin.Geolocator;
using System.Linq;
using VinaDelivery.Models;
using VinaDelivery.Providers;
using VinaDelivery.Utils;

namespace VinaDelivery.ViewModels.Templates
{
    public class CompleteDeliveryPopupViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private readonly IStorageProvider _storageProvider;
        private CustomerDeliveryViewModel _delivery;

        public CustomerDeliveryViewModel Delivery
        {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        public Stream SignatureImageStream { get; set; }

        public DelegateCommand CompleteCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        public CompleteDeliveryPopupViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            _storageProvider = Xamarin.Forms.DependencyService.Get<IStorageProvider>();
            CompleteCommand = new DelegateCommand(async () => await ExecuteCompleteCommandAsync());
            CancelCommand = new DelegateCommand(async () => await ExecuteCancelCommandAsync());
        }

        private async Task ExecuteCompleteCommandAsync()
        {
            try
            {
                if(SignatureImageStream == null)
                {
                    await UserDialogs.Instance.AlertAsync("Vui lòng ký tên xác nhận!", "Chú ý");
                    return;
                }

                await ((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();

                var appData = AppService.AppDataStorage.GetAppData();

                var deliveries = Delivery.GroupingDeliveryItems.Select(x => x.Key).ToList();
                var fileName = _storageProvider.SaveFile(SignatureImageStream, $"Signature_{DateTime.Now.Ticks}.png");
                deliveries.ForEach(x => x.SignatureFileName = fileName);
                deliveries.ForEach(x => x.Status = x.Items.All(m => m.IsFullDelivered) ?
                                                    new DeliveryStatus { Id = (int)DeliveryStatusEnum.Done }
                                                    : new DeliveryStatus { Id = (int)DeliveryStatusEnum.PartialDone });

                appData.NeedSync = true;
                appData.CurrentTrip = App.Current.Trip;
                AppService.AppDataStorage.SaveAppData(appData);

                // trigger background service to sync trip
                MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

                await UserDialogs.Instance.AlertAsync("Hoàn tất giao hàng !", "Thông báo");

                App.Current.MainPage = new RootPage() { Detail = new IconNavigationPage(new TripTrackingPage()) };
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        private async Task ExecuteCancelCommandAsync()
        {
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();
        }
    }
}
