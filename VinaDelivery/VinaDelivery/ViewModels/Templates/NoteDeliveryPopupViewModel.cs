﻿using Acr.UserDialogs;
using Plugin.Iconize;
using Prism.Commands;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Utils;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Templates;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.ViewModels.Templates
{
    public class NoteDeliveryPopupViewModel : ViewModelBase
    {
        private CustomerDeliveryViewModel _delivery;

        public CustomerDeliveryViewModel Delivery
        {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        private string _note;
        public string Note
        {
            get => _note;
            set => SetProperty(ref _note, value);
        }

        public NoteFor NoteFor { get; set; }

        public DelegateCommand NoteCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        public NoteDeliveryPopupViewModel(IAppService appService) : base(appService)
        {
            NoteCommand = new DelegateCommand(async () => await ExecuteNoteCommandAsync());
            CancelCommand = new DelegateCommand(async () => await ExecuteCancelCommandAsync());
        }

        private async Task ExecuteNoteCommandAsync()
        {
            try
            {
                await ((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();
                Delivery.Note = _note;
                if (NoteFor == NoteFor.CompleteDelivery)
                {
                    var deliveries = Delivery.GroupingDeliveryItems.Select(x => x.Key).Where(x => x.Items.Any(y => y.IsDeliveryDone)).ToList();
                    deliveries.ForEach(x => x.Note = _note);
                    await ((RootPage)App.Current.MainPage).Detail.Navigation.PushPopupAsync(new CompleteDeliveryPopup(_delivery));
                }

                if(NoteFor == NoteFor.StopDelivery)
                {
                    var appData = AppService.AppDataStorage.GetAppData();

                    var deliveries = Delivery.GroupingDeliveryItems.Select(x => x.Key).ToList();
                    deliveries.ForEach(x => x.Note = _note);
                    deliveries.ForEach(x => x.Status = new DeliveryStatus { Id = (int)DeliveryStatusEnum.Failed });

                    appData.NeedSync = true;
                    appData.CurrentTrip = App.Current.Trip;
                    AppService.AppDataStorage.SaveAppData(appData);

                    // trigger background service to sync trip
                    MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

                    await UserDialogs.Instance.AlertAsync("Dừng giao hàng!", "Thông báo");

                    App.Current.MainPage = new RootPage() { Detail = new IconNavigationPage(new TripTrackingPage()) };
                }
            }
            catch (Exception e)
            {
                await UserDialogs.Instance.AlertAsync("Có lỗi xảy ra. Vui lòng thử lại.", "Thông báo");
            }
        }

        private async Task ExecuteCancelCommandAsync()
        {
            await((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();
            Note = string.Empty;
        }
    }

    public enum NoteFor
    {
        Normal, CompleteDelivery, StopDelivery
    }
}
