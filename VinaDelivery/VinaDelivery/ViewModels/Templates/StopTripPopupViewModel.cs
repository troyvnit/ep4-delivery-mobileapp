﻿using Acr.UserDialogs;
using Prism.Commands;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Plugin.Iconize;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Utils;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using Xamarin.Forms;
using static VinaDelivery.ViewModels.Trip.TripConfirmationPageViewModel;

namespace VinaDelivery.ViewModels.Templates
{
    public class StopTripPopupViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private CustomerDeliveryViewModel _delivery;

        public CustomerDeliveryViewModel Delivery
        {
            get => _delivery;
            set => SetProperty(ref _delivery, value);
        }

        private string _reasonText;
        public string ReasonText
        {
            get => _reasonText;
            set => SetProperty(ref _reasonText, value);
        }

        public DelegateCommand StopCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        public StopTripPopupViewModel(IAppService appService, ITripService tripService) : base(appService)
        {
            _tripService = tripService;
            StopCommand = new DelegateCommand(async () => await ExecuteStopCommandAsync());
            CancelCommand = new DelegateCommand(async () => await ExecuteCancelCommandAsync());
        }

        private async Task ExecuteStopCommandAsync()
        {
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();

            App.Current.Trip.StopReason = _reasonText;
            App.Current.Trip.Status = new TripStatus { Id = (int)TripStatusEnum.Stop };

            var appData = AppService.AppDataStorage.GetAppData();
            appData.IsCurrentTripCompleted = true;
            appData.NeedSync = true;
            appData.CurrentTrip = App.Current.Trip;
            AppService.AppDataStorage.SaveAppData(appData);

            ReasonText = string.Empty;

            // trigger background service to sync trip
            MessagingCenter.Send(new SyncTripMessage(), "SyncTrip");

            await UserDialogs.Instance.AlertAsync($"Bạn đã dừng chuyến {App.Current.Trip.Code}!", "Thông báo");

            App.Current.MainPage = new RootPage() { Detail = new IconNavigationPage(new TripDonePage()) };
        }

        private async Task ExecuteCancelCommandAsync()
        {
            await ((RootPage)App.Current.MainPage).Detail.Navigation.PopPopupAsync();
            ReasonText = string.Empty;
        }
    }
}
