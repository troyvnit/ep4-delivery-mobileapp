﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Iconize;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using VinaDelivery.Data;
using VinaDelivery.Models;
using VinaDelivery.Services;
using VinaDelivery.Services.Base;
using VinaDelivery.Services.Identity;
using VinaDelivery.ViewModels.Base;
using VinaDelivery.Views;
using VinaDelivery.Views.Trip;
using VinaDelivery.Utils;
using Xamarin.Forms;
using Microsoft.AppCenter;

namespace VinaDelivery.ViewModels.Account
{
    public class LoginPageViewModel : ViewModelBase
    {
        private readonly ITripService _tripService;
        private readonly IIdentityService _identityService;

        private string _loginStatusText;
        public string LoginStatusText
        {
            get => _loginStatusText;
            set => SetProperty(ref _loginStatusText, value);
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public DelegateCommand LoginCommand { get; set; }

        public LoginPageViewModel(IAppService appService, ITripService tripService, IIdentityService identityService) : base(appService)
        {
            _tripService = tripService;
            _identityService = identityService;
            LoginCommand = new DelegateCommand(async () => await ExecuteLoginCommandAsync());
        }

        private async Task ExecuteLoginCommandAsync()
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                return;

            IsBusy = true;

            LoginStatusText = "ĐĂNG NHẬP...";
            var result = await _identityService.GetTokenAsync(Username, Password);
            if (result != null)
            {
                Settings.AccessToken = result.AccessToken;
                Settings.AccessTokenExpiresDate = result.AccessTokenExpiresDate;
                Settings.Username = Username;
                Settings.Password = Password;
                Settings.FullName = result.FullName;

                LoginStatusText = "ĐỒNG BỘ DỮ LIỆU...";
                await GetActiveTripAsync();

                IsBusy = false;
            }
            else
            {
                await UserDialogs.Instance.AlertAsync("Không thể đăng nhập. Vui lòng kiểm tra lại tài khoản", "Thông Báo", "OK");
            }


            IsBusy = false;
        }

        public async Task LoadDataAsync()
        {
            IsBusy = true;

            LoginStatusText = "ĐỒNG BỘ DỮ LIỆU...";
            await GetActiveTripAsync();

            IsBusy = false;
        }

        private async Task GetActiveTripAsync()
        {
            await _tripService.GetActiveTripAndSaveToStorageAsync(AppService);
        }
    }
}
