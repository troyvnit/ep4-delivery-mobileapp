﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VinaDelivery
{
    public class AppConfig
    {
#if DEBUG
        public static string API_BASE_URL = "https://e-delivery-api.azurewebsites.net/api";
        public const string IDENTITY_PROVIDER = "https://e-delivery-is.azurewebsites.net";
#else
        public static string API_BASE_URL = "https://e-delivery-api.azurewebsites.net/api";
        public const string IDENTITY_PROVIDER = "https://e-delivery-is.azurewebsites.net";
#endif
    }
}
